package ru.vsev.kotlingo.ui

import android.support.v7.app.AppCompatActivity

public open class BaseActivity : AppCompatActivity()