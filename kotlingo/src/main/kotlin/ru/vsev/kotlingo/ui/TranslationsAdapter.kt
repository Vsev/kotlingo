package ru.vsev.kotlingo.ui

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import ru.vsev.kotlingo.R
import ru.vsev.kotlingo.data.api.TranslationResult
import java.util.*

public class TranslationsAdapter(var translations : List<Pair<TranslationResult, TranslationResult>>) : RecyclerView.Adapter<TranslationsAdapter.ViewHolder>() {

    companion object {
        val EMPTY_LIST = ArrayList<Pair<TranslationResult, TranslationResult>>()
    }

    private class ViewHolder(itemView: View?) : RecyclerView.ViewHolder(itemView) {       
        val wordType = itemView!!.findViewById(R.id.wordType) as TextView
        val translationLeft = itemView!!.findViewById(R.id.left_result) as TextView
        val translationRight = itemView!!.findViewById(R.id.right_result) as TextView
        val metaLeft = itemView!!.findViewById(R.id.left_meta) as TextView
        val metaRight = itemView!!.findViewById(R.id.right_meta) as TextView
    }

    override fun getItemCount(): Int {
        return translations.size()
    }

    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): ViewHolder? {
        val view = LayoutInflater.from(parent!!.context)
                .inflate(R.layout.translation_item, parent, false)

       return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder?, position: Int) {
        if (holder == null)
            throw NullPointerException("view holder is null")
        val translationPair = translations[position]
        val leftResult = translationPair.first
        val rightResult = translationPair.second
        holder.wordType.text = leftResult.wordType
        holder.translationLeft.text = leftResult.translation
        holder.metaLeft.text = leftResult.meta
        holder.translationRight.text = rightResult.translation
        holder.metaRight.text = rightResult.meta
    }

    fun clear() {
        translations = EMPTY_LIST
        notifyDataSetChanged()
    }
}
