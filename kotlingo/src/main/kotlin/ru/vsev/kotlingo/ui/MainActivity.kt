package ru.vsev.kotlingo.ui

import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.View
import android.widget.EditText
import android.widget.ImageButton
import android.widget.ProgressBar
import com.squareup.okhttp.OkHttpClient
import ru.vsev.kotlingo.R
import ru.vsev.kotlingo.data.api.BablaService
import ru.vsev.kotlingo.data.api.TranslationParser
import ru.vsev.kotlingo.utils.bindView
import rx.android.schedulers.AndroidSchedulers
import rx.schedulers.Schedulers
import rx.subscriptions.CompositeSubscription
import timber.log.Timber

public class MainActivity : BaseActivity() {

    companion object {
        val EN_RU = "английский – русский"
        val RU_EN = "русский – английский"
    }

    val searchTextView by bindView<EditText>(R.id.search_phrase)
    val clearSearch by bindView<ImageButton>(R.id.action_clear_text)
    val progress by bindView<ProgressBar>(R.id.progress)
    val searchResults by bindView<RecyclerView>(R.id.translations)

    val subscriptions = CompositeSubscription()

    val translationsAdapter = TranslationsAdapter(TranslationsAdapter.EMPTY_LIST)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.main_activity)
        initRecyclerView()
        supportActionBar.title = EN_RU

        val service = BablaService(OkHttpClient())

        searchTextView.setOnEditorActionListener { textView, i, keyEvent ->
            searchResults.hide()
            translationsAdapter.clear()
            progress.show()
            subscriptions.add(service.translateRx("${searchTextView.text}") // TODO ?
                    .subscribeOn(Schedulers.newThread())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe ({ supportActionBar.title = if (it.lang == TranslationParser.LANG_EN) EN_RU else RU_EN
                                translationsAdapter.translations = it.translations
                                translationsAdapter.notifyDataSetChanged()
                            },
                            { throwable -> // onError
                                Timber.e(throwable, "search error")
                                searchResults.show()
                                progress.hide() },
                            { // onComplete
                                searchResults.show()
                                progress.hide()
                                Timber.d("COMPLETE")
                            }
                    )
            )
            true
        }
        clearSearch.setOnClickListener { searchTextView.setText("") } //TODO cancel network request
    }

    private fun initRecyclerView() {
        val layoutManager = LinearLayoutManager(this)
        searchResults.layoutManager = layoutManager
        searchResults.adapter = translationsAdapter
    }

    override fun onDestroy() {
        super.onDestroy()
        subscriptions.unsubscribe()
    }

    private fun View.hide() {
        this.visibility = View.INVISIBLE
    }

    private fun View.show() {
        this.visibility = View.VISIBLE
    }
}
