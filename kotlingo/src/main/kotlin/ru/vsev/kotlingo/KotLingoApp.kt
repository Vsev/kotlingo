package ru.vsev.kotlingo

import android.app.Application
import timber.log.Timber

class KotLingoApp : Application() {
    override fun onCreate() {
        super.onCreate()
        if (BuildConfig.DEBUG) {
            Timber.plant(Timber.DebugTree())
        }
    }
}
