package ru.vsev.kotlingo.data.api

import com.squareup.okhttp.OkHttpClient
import com.squareup.okhttp.Request
import rx.Observable
import java.io.IOException

public class BablaService(val client : OkHttpClient) {
    companion object {
        private val BASE_URL = "http://www.babla.ru/"
        private val EN_RU = "английский-русский"
        private val RU_EN = "русский-английский"

        fun buildUrl(wordToTranslate : String, direction: String = EN_RU) : String {
            val sb = StringBuilder(BASE_URL.length() + direction.length() + wordToTranslate.length() + 1)
            sb.append(BASE_URL)
                    .append(direction)
                    .append("/")
                    .append(wordToTranslate)
            return sb.toString()
        }
    }

    @Throws(IOException::class)
    fun translate(wordToTranslate : String) : String  {
        val request = Request.Builder()
                .url(buildUrl(wordToTranslate))
                .build()

        val response = client.newCall(request).execute()
        return response.body().string()
    }

    fun translateRx(wordToTranslate : String) : Observable<TranslationResponse> {
        return Observable.create<TranslationResponse> { subscriber ->
            try {
                val translationHTML = translate(wordToTranslate)
                subscriber.onNext(TranslationParser.parseTranslationHTML(translationHTML))
                subscriber.onCompleted()
            } catch (e: IOException) {
                subscriber.onError(e)
            } catch (e: NullPointerException) {
                subscriber.onError(e)
            }
        }
    }
}
