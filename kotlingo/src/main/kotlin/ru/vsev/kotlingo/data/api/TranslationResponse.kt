package ru.vsev.kotlingo.data.api

public class TranslationResponse(
        val translations : List<Pair<TranslationResult, TranslationResult>>,
        @TranslationParser.Language val lang : Long)
