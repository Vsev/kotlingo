package ru.vsev.kotlingo.data.api

public class TranslationResult(val translation: String, val wordType: String, val meta: String)
