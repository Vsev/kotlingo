package ru.vsev.kotlingo.data.api

import android.support.annotation.IntDef
import org.jsoup.Jsoup
import org.jsoup.nodes.Element
import kotlin.text.Regex

public class TranslationParser {

    @Retention(AnnotationRetention.SOURCE)
    @IntDef(LANG_EN, LANG_RU)
    annotation class Language

    companion  object {

        val curlyRegex = Regex("\\{(.*?)\\}")
        val EN = "английский"
        val RU = "русский"

        const val LANG_EN = 0L
        const val LANG_RU = 1L

        fun parseTranslationHTML(html: String) : TranslationResponse {
            val doc = Jsoup.parse(html)
            val title = doc.head().getElementsByTag("title").text()
            val language = if (title.contains(EN)) LANG_RU else LANG_EN
            val translationSection = doc.select("section:contains(Точный англо-русский перевод)").first()
                ?: doc.select("section:contains(Точный русско-английский перевод)").first()
            val resultRows = translationSection.select("div.row-fluid.result-row")
            val translations = resultRows.map {
                val resultLeft = parseElementByClass(it, "div.span6.result-left")
                val resultRight = parseElementByClass(it, "div.span6.result-right")
                Pair(resultLeft, resultRight)
            }.filter { it.first != null && it.second != null}
            .map { Pair(it.first!!, it.second!!) } // it functional though..
            return TranslationResponse(translations, language)
        }

        private fun parseElementByClass(element: Element, cssQuery : String): TranslationResult? {
            val result = element.select(cssQuery).first()
            val additionalInfo = result.getElementsByTag("span").first()
            val wordType = additionalInfo?.getElementsByTag("abbr")?.first()?.attr("title") ?: ""

            val addInfo = additionalInfo?.text() ?: ""
            val meta = addInfo.replace(curlyRegex, "")

            val link = result.getElementsByClass("result-link").first()
            return if (link != null && link.text() != null) {
                TranslationResult(link.text(), wordType, meta)
            } else
                null
        }
    }




}
