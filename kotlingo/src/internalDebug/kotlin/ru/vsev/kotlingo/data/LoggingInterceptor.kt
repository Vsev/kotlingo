package ru.vsev.kotlingo.data

import com.squareup.okhttp.Interceptor
import com.squareup.okhttp.Response
import timber.log.Timber

@SuppressWarnings("unused")
public class LoggingInterceptor : Interceptor {
    override fun intercept(chain : Interceptor.Chain) : Response {
        val request = chain.request()

        val t1 = System.nanoTime()
        Timber.i("Sending request %s on %s%n%s",
                request.url(), chain.connection(), request.headers())

        val response = chain.proceed(request);

        val t2 = System.nanoTime()
        Timber.i("Received response for %s in %.1fms%n%s",
                response.request().url(), (t2 - t1) / 1e6, response.headers())

        return response
    }
}
